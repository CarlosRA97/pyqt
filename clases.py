class Animal(object):

    def __init__(self, name, age):
        """

        :rtype: object
        """
        self.name = name
        self.age = age


    def comer(self, comida):

        if comida == None or comida == "":
            print "No le has dado comida al %s" % self.especie
            return False
        else:
            print "el %s esta comiendo %s" % (self.especie, comida)
            return True


    def dormir(self):
        print "el %s esta durmiendo" % (self.especie)
        return True


    def levantarse(self):
        print 'el %s se levanta' % (self.especie)
        return True

import datetime

from clases import Animal


class Perro ( Animal ):

	def __init__ ( self, name, age, especie ):
		self.especie = especie
		self.name = name
		self.age = age
		self.hour = datetime.datetime.now ( ).hour

		super ( Perro, self ).__init__ ( age, name )

	def do_not_sleep ( self, alimento ):
		if self.comer ( alimento ) == True and self.hour in range ( 20, 24 ):
			print "El %s esta comiendo, no puede dormir" % self.especie
		else:
			print "No esta comiendo puede dormir"


manolito = Perro ( "juan", 2, "perro" )

manolito.do_not_sleep ( "atun" )
